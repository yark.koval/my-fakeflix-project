import React, { useRef } from 'react';
import { useState } from 'react';
import firebase from 'firebase/app';
import { auth } from "../firebase";
import './SignIn.css';
import './UserBlock.css';




require("firebase/auth");

function UserBlock() {
    const [shouldShowElem, setshouldShowElem] = useState(false);
    return (
        <div className="userblock">
           <button className="user_btn">{firebase.auth().currentUser.email}</button>
            <button to='' className="logout-btn" onClick={() => firebase.auth().signOut()}>Sign out</button>
        </div>
    )
}

export default UserBlock
