import React, { useRef }  from 'react';
import {auth} from "../firebase";
import './SignIn.css';

require("firebase/auth");


const SignIn = () => {
    let emailRef = useRef(null);
    let passwordRef = useRef(null);
    const signUp = (e) => {
        e.preventDefault();
        auth.createUserWithEmailAndPassword(
            emailRef.current.value, passwordRef.current.value).then(user =>{
                console.log(user);
            
        }).catch(err => {
            console.log(err);
        });
    };
    const signIn = (e) => {
        e.preventDefault();
        auth.signInWithEmailAndPassword(
            emailRef.current.value, passwordRef.current.value).then(user => {
                console.log(user);                
            }).catch(err => {
                console.log(err);
                alert('This user not found. Please register before sign in');
            });
    };
    return (
        <div className="signin">
           <form action="">
                <h1 className="signin__title">Sign in</h1>
                <input type="email" ref={emailRef} name="" id="email" placeholder="email"/>
                <input type="password" ref={passwordRef} name="" id="password" placeholder="password"/>
                <button className="signin__btn" onClick={signIn}>Sign in</button>
                <button className="signin__btn signup__btn" onClick={signUp} >Sign Up</button>
                <h6 className="signup__text">Not yet registered? Type your email, create password and click "Sign Up" </h6>
               </form> 
        </div>
    )
}

export default SignIn
