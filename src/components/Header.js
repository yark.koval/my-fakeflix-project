import React from "react";
import './Header.css';
import firebase from 'firebase/app';
import logo from '.././Fakeflix_logo.png';
import UserBlock from '.././components/UserBlock';
import SignIn from '.././components/SignIn';

const Header = () => {
    return ( 
        <header className = "App-header" >
        <div className = "logo" > < img src = { logo }
        alt = "" / > </div>
         {} 
         { firebase.auth().currentUser ? < UserBlock / > : < SignIn / > }
        </header>
    );
};


export default Header;