import React from "react";
import './Footer.css';
import logo from '.././Footer_logo.png';

const Footer = () => {
    return (
        <footer>
            <div className="footer_logo"><img src={logo} alt="" /></div>
            <p>©All rights reserved.</p>
            <p>Media center</p>
            <p>Terms of use</p>
            <p>Contact us</p>
        </footer>
    );
};



export default Footer;