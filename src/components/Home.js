import React from 'react';
import firebase from 'firebase/app';
import './Home.css';

const Home = () => {
    
    return firebase.auth().currentUser ? (
        <div className="home">
            <div className="info_block">
                <p>Netflix provides a personalized subscription service that allows our members to access entertainment content (“Netflix content”) over the Internet on certain Internet-connected TVs, computers and other devices ("Netflix ready devices").</p>
            </div>
            
            <div className="info_block">
               <p>These Terms of Use govern your use of our service. As used in these Terms of Use, "Netflix service", "our service" or "the service" means the personalized service provided by Netflix for discovering and accessing Netflix content, including all features and functionalities, recommendations and reviews, our websites, and user interfaces, as well as all content and software associated with our service.</p>
            </div>
            <div className="info_block">
               <p>Netflix software is developed by, or for, Netflix and may solely be used for authorized streaming and to access Netflix content  through Netflix ready devices. This software may vary by device and medium, and functionalities and features may also differ between devices. You acknowledge that the use of the service may require third party software that is subject to third party licenses. You agree that you may automatically receive updated versions of the Netflix software and related third-party software.</p>
            </div>
            
        </div>
    )
    : <div/>
}

export default Home


