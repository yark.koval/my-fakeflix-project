import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
// import * as firebase from 'firebase';
import firebase from 'firebase/app';
// import { initializeApp } from 'firebase/app';
// import * as firebase from "@firebase/app";

// const firebaseConfig = {
//     apiKey: "AIzaSyB1B2KLeiZxPwWZ6dHEPmPShAK8JWVa9hw",
//     authDomain: "my-fakeflix-project.firebaseapp.com",
//     databaseURL: "https://my-fakeflix-project-default-rtdb.europe-west1.firebasedatabase.app",
//     projectId: "my-fakeflix-project",
//     storageBucket: "my-fakeflix-project.appspot.com",
//     messagingSenderId: "322797126883",
//     appId: "1:322797126883:web:a1102b106d9981f1c5b1d6"
// };

// firebase.initializeApp(firebaseConfig);

ReactDOM.render( <React.StrictMode >
    <App / >
    </React.StrictMode>,
    document.getElementById('root')
);
