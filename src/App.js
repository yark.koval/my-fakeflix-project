import './App.css';
import './components/Header';

import React from 'react';
import { useState } from 'react';
import { useEffect } from 'react';
import { BrowserRouter } from 'react-router-dom';
import { Redirect, Route, Switch } from 'react-router';
import firebase from 'firebase/app';
import { Link } from "react-router-dom";
import { auth } from './firebase';

import logo from '.././src/Fakeflix_logo.png';

import SignIn from './components/SignIn';
import Header from './components/Header';
import Footer from './components/Footer';
import UserBlock from './components/UserBlock';
import Home from './components/Home';

require('firebase/database');
require('firebase/auth');

export default App;

function App(props) {
  
  const [shouldShowElem, setshouldShowElem] = useState(false);
  const [user, setUser] = useState(null);
  useEffect(() => {
    const unsubscribe= auth.onAuthStateChanged(userAuth=>{
      const user ={
        uid : userAuth?.uid,
        email: userAuth?.email
      };
      if(userAuth){
        console.log(userAuth);
        setUser(user);
      }
      else{
        setUser(null);
      }
    })
    return unsubscribe;
  }, []);

  return (
    <BrowserRouter>
      <div className="App">
        <Header/>
        <Switch>
          {/* <Route exact path='/'>
            <Redirect to='/home'/>
          </Route> */}
          <Route exact path='/home' component={Home}/>
          {/* <Route exact path='/tv-shows'component={}/>
          <Route exact path='/login' component=}/>
          <Route exact path='/register' component={}/>
          <Route exact path='/room' render={(props) => <component {...props}/>}/>
          <Route exact path='/shared' component={}/> */}
        </Switch>
        <Footer/>
      </div>
    </BrowserRouter>
    
  );
}


